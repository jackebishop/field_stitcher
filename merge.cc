void merge() {
	const double x_offset=0.0035;
	const double y_offset=0.0035;
	TH2F *hmapE = new TH2F("hmapE","",29*2-1,-2.*x_offset,2.*x_offset,29*2-1,-2.*y_offset,2.*y_offset);
//Merged file
	ofstream outs;
	outs.open("merged2.example");
//Read in file center
	ifstream incenter;
	incenter.open("center.example");
	int entries=0;
	int count1=0;
	while(incenter.good()) {
		double x_pos,y_pos,z_pos,xE,yE,zE,xV,yV,zV;
		incenter>>x_pos>>y_pos>>z_pos>>xE>>yE>>zE>>xV>>yV;
//		cout<<x_pos-x_offset<<"\t"<<y_pos-y_offset<<"\t"<<z_pos<<endl;
		hmapE->Fill(x_pos-x_offset,y_pos-y_offset,1);
		outs<<x_pos-x_offset<<" "<<y_pos-y_offset<<" "<<z_pos<<" "<<xE<<" "<<yE<<" "<<zE<<" "<<xV<<" "<<yV<<endl; //(0,0)
		count1++;
		entries++;
	}
	cout<<"Entries "<<entries<<endl;
//Read in file up
	ifstream inup;
	inup.open("up.example");
	int count2=0;
	while(inup.good()) {
		double x_pos,y_pos,z_pos,xE,yE,zE,xV,yV,zV;
		inup>>x_pos>>y_pos>>z_pos>>xE>>yE>>zE>>xV>>yV;
		if(y_pos!=-y_offset && x_pos!=x_offset)hmapE->Fill(x_pos-x_offset,y_pos+y_offset,300);
		if(x_pos!=-x_offset && y_pos!=y_offset)hmapE->Fill(x_pos+x_offset,y_pos-y_offset,300);
//		if(x_pos!=-x_offset)hmapE->Fill(x_pos+x_offset,y_pos-y_offset,3);
		count2++;
		if(y_pos!=-y_offset && x_pos!=x_offset) {
			outs<<x_pos-x_offset<<" "<<y_pos+y_offset<<" "<<z_pos<<" "<<xE" "<<yE" "<<zE" "<<xV" "<<yVendl;//(0,1)
			entries++;
		}
		if(x_pos!=-x_offset && y_pos!=y_offset) {
			outs<<x_pos+x_offset<<" "<<y_pos-y_offset<<" "<<z_pos<<" "<<xE" "<<yE" "<<zE" "<<xV" "<<yVendl;//(1,0)
			entries++;
		}
	}
	cout<<"Entries "<<entries<<endl;

//Read in file side
	ifstream inside;
	inside.open("side.example");
	int count3=0;
	while(inside.good()) {
		double x_pos,y_pos,z_pos,xE,yE,zE,xV,yV,zV;
		inside>>x_pos>>y_pos>>z_pos>>xE>>yE>>zE>>xV>>yV;
		hmapE->Fill(x_pos+x_offset,y_pos+y_offset,20);
		if((x_pos+x_offset!=0 || y_pos+y_offset!=0)) {
			outs<<x_pos+x_offset<<" "<<y_pos+y_offset<<" "<<z_pos<<" "<<xE<<" "<<yE<<" "<<zE<<" "<<xV<<" "<<yV<<endl;//(1,1)
			entries++;
		}
		count3++;
	}
	cout<<"Entries "<<entries<<endl;

	cout<<"COUNTS: "<<count1<<"\t"<<count2<<"\t"<<count3<<"\t"<<entries<<endl;
	outs.close();
	hmapE->Draw("COLZ");
}
